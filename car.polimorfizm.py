class Car():
    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year
        self.odometer_reading = 0

    def gas_station(self):
        pass

    def get_info(self):
        return "Модель {},Год {},Сделано в {},".format(self.model,self.year,self.make)

class Fit(Car):
    def gas_station(self):
        return "petrol"


class Tesla(Car):
    def gas_station(self):
        return "electricity"

Fit = Fit('Японии','Хонда','2000')
Tesla = Tesla('Америке','Х','2018')
print(Fit.gas_station())
print(Fit.get_info())
print(Tesla.gas_station())
print(Tesla.get_info())





